fun main(args: Array<String>){
    for(i in 1..10) {
        val fortune = getFortuneCookie(getBirthday())
        println(fortune)
        if(fortune.startsWith("Take it easy", true))
            break
    }
}

fun getBirthday(): Int {
    print("Enter your birthday: ")
    return readLine()?.toIntOrNull() ?: 1
}

fun getFortuneCookie(birthday: Int): String {
    val fortunes = listOf(
        "You will have a great day!",
        "Things will go well for you today.",
        "Enjoy a wonderful day of success.",
        "Be humble and all will turn out well.",
        "Today is a good day for exercising restraint.",
        "Take it easy and enjoy life!",
        "Treasure your friends because they are your greatest fortune."
    )

    return when (birthday) {
        in 1..7 -> fortunes[1]
        28, 31 -> fortunes[2]
        else -> fortunes[birthday % fortunes.size]
    }
}