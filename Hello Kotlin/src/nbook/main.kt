package nbook

fun main(args: Array<String>) {
    val book = Book("The Lord of the Rings", "J.R.R. Tolkien", 1954, 1000)
    book.printUrl()

    val puppy = Puppy()
    puppy.playWithBook(book)

    println(book.pages)
}