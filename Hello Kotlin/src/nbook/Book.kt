package nbook

const val MAX_BOOKS = 30

class Book (val title: String, val author: String, val year: Int, var pages: Int){

    companion object Constants {
        const val BASE_URL: String = "http://lallero.com/"
    }

    fun getPair() = title to author

    fun getTriple() = Triple(title, author, year)

    fun canBorrow(hasBooks: Int): Boolean {
        return hasBooks < MAX_BOOKS;
    }

    fun printUrl(){
        println(Constants.BASE_URL + title.replace(' ', '+') + ".html")
    }
}

fun Book.weigth(): Double = pages.times(1.5)

fun Book.tornPage(newPages: Int) = run { pages = newPages }