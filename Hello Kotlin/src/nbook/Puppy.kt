package nbook

import java.util.*

class Puppy {
    fun playWithBook(book: Book) = book.tornPage(book.pages - Random().nextInt(book.pages))
}