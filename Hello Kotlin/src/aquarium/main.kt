package aquarium

fun main(args: Array<String>){
    buildAquarium()
}

fun buildAquarium(){
    val myAquarium = Aquarium()

    println("length: ${myAquarium.length}\n" +
            "height: ${myAquarium.height}\n" +
            "width: ${myAquarium.width}")

    myAquarium.length = 80

    println("length: ${myAquarium.length}\n")

    println("volume: ${myAquarium.volume} litres")

    myAquarium.volume = 100
    println("length: ${myAquarium.length}\n" +
            "height: ${myAquarium.height}\n" +
            "width: ${myAquarium.width}\n" +
            "volume: ${myAquarium.volume}")

    val secondAquarium = Aquarium(numberOfFish = 9)
    println("length: ${secondAquarium.length}\n" +
            "height: ${secondAquarium.height}\n" +
            "width: ${secondAquarium.width}\n" +
            "volume: ${secondAquarium.volume}")
}