package aquarium

interface AquariumAction {
    fun eat()
    fun jump()
    fun clean()
    fun catchFish()
    fun swim() {
        println("swim")
    }
}

abstract class AquariumFish: FishAction {
    abstract val color: String
    override fun eat() = println("yum")
}

fun main(args: Array<String>) {
    val pleco = Plecostomus()
    println("Fish has color ${pleco.color}")
    pleco.eat()
}

interface FishAction {
    fun eat()
}

interface FishColor {
    val color: String
}

class Plecostomus(fishColor: FishColor = GoldColor):
    FishAction by PrintingFishAction("a lot of algae"), /// interface delegation
    FishColor by GoldColor /// interface delegation

/// singleton
object GoldColor: FishColor {
    override val color = "gold"
}
/// singleton
object RedColor: FishColor {
    override val color = "red"
}

class PrintingFishAction(val food: String): FishAction {
    override fun eat() {
        println(food)
    }
}