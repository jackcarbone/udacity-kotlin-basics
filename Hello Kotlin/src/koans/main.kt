package koans

fun main(args: Array<String>) {
    println(containsEven(listOf(1, 2, 3, 4)))
}

fun containsEven(collection: Collection<Int>): Boolean = collection.any { it % 2 == 0}