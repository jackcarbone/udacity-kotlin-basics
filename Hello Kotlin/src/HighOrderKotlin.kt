/// high order function: funzione che prende un'altra funzione come parametro
/// sia x una funzione che prende un intero e restituisce un intero: x: (Int) -> Int
val x: (Int) -> Int = {
    /* parametro della funzione */ parametro ->
    /* corpo della funzione */ parametro * 2
}
/// in particolare la funzione x prende un intero e lo moltiplica per 2.

/// definiamo ora una funzione y che prende come argomento un intero n e una funzione generica f del tipo
/// (Int) -> Int e restituisce il risultato dell'applicazione f(n)
fun y(n: Int, f: (Int) -> Int): Int{
    return f(n)
}

/// quindi e invochiamo l'istruzione y(2, x) il risultato atteso sarà 4.

/// Qual è la cosa figa in tutto ciò? Tutte le funzioni che rispettano il tipo (Int) -> Int (e quindi che
/// prendono un Int e resituiscono un Int posso essere usate come parametro di y. Il fatto che siano usate
/// come parametro vuol dire che
///     1. la funzione deve essere passata senza parametri
///     2. la funzione parametro verrà invocata (se necessario) all'interno della funzione chiamante

/// funzione identità
val i: (Int) -> Int = {
    x -> x
}

/// funzione 'quadrato'
val q: (Int) -> Int = {
    x -> x*x
}

/// funzione costante
val c: (Int) -> Int = {
    1
}

fun main(args: Array<String>){
    println(y(2, x))
    println(y(2, i))
    println(y(2, q))
    println(y(2, c))
}