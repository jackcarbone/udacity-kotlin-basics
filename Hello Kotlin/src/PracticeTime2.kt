import java.util.*

fun main(args: Array<String>){
    /// println(whatShouldIDoToday("sad"))
    /// repeat(12) {println(rollDice(6))}
    gamePlayWithLambda(rollDice2)
}

fun whatShouldIDoToday(mood: String, weather: String = "sunny", temperature: Int = 24): String {
    return when{
        isHappySunny(mood,

            weather) -> "go for a walk"
        isSadRainyCold(mood, weather, temperature) -> "stay in bed"
        isVeryHot(temperature) -> "go swimming"
        else -> "Stay home and read."
    }
}

fun isHappySunny(mood: String, weather: String) = mood == "happy" && weather == "sunny"
fun isVeryHot(temperature: Int) = temperature > 35
fun isSadRainyCold(mood: String, weather: String, temperature: Int) = mood == "sad" && weather == "rainy" && temperature == 0

/// lambda (function notation)
val rollDice: (Int) -> Int = {
    x ->
    if(x ==0) 0
    else Random().nextInt(x) + 1
}

/// lambda (function notation)
val rollDice2 = {
    x: Int ->
    if(x ==0) 0
    else Random().nextInt(x) + 1
}

fun gamePlay(roll: Int){
    println(roll)
}

fun gamePlayWithLambda(roller : (Int) -> Int) = println(roller(6))