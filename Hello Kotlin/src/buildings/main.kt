package buildings

fun main(args: Array<String>) {
    makeWood()
}

fun makeWood(){
    val wood = Wood()
    Building(wood).build()
}