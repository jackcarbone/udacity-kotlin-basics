package buildings

open class BaseBuildingMaterial(var numberNeeded: Int  = 1)

class Wood : BaseBuildingMaterial(4)

class Brick : BaseBuildingMaterial(8)

class Building<T: BaseBuildingMaterial>(val material: T){
    var baseMaterialsNeeded: Int = 100

    var actualMaterialsNeeded = baseMaterialsNeeded * material.numberNeeded

    fun build(){
        println("$actualMaterialsNeeded ${material::class.simpleName} required")
    }
}

