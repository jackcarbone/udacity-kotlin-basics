import java.util.*

fun main(args: Array<String>){

    feedThefish()

    println("Expected 'false' got " + canAddFish(10.0, listOf(3,3,3))) /// ---> false
    println("Expected 'true' got " + canAddFish(8.0, listOf(2,2,2), hasDecorations = false)) ///---> true
    println("Expected 'false' got " + canAddFish(9.0, listOf(1,1,3), 3)) ///---> false
    println("Expected 'true' got " + canAddFish(10.0, listOf(), 7, true)) ///---> true
}

/// arguments w/out default should called first
fun shouldChangeWater(day: String, temp: Int = 22, dirty: Int = 20): Boolean {

    return when {
        isTooHot(temp) -> true
        isDirty(dirty) -> true
        isSunday(day)  -> true
        else -> false
    }
}

fun isTooHot(temp: Int) = temp > 30
fun isDirty(dirty: Int) = dirty > 30
fun isSunday(day: String) = day == "Sunday"

fun feedThefish(){
    val day = randomDay()
    val food = fishFood(day)

    println("Today is $day and the fish eat $food")
}

fun fishFood(day: String) : String {
    return when(day) {
        "Monday"    -> "flakes"
        "Wednesday" -> "redworms"
        "Thursday"  -> "granules"
        "Friday"    -> "mosquitoes"
        "Saturday"  -> "lettuce"
        "Sunday"    -> "plankton"
        else        -> "fasting"
    }
}

fun randomDay(): String {
    val week = listOf("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")

    return week[Random().nextInt(7)]
}

/// rule one-inch-per-fish-per-gallonofwater
fun canAddFish(tankSize: Double, currentFish: List<Int>, fishSize: Int = 2, hasDecorations: Boolean = true): Boolean {
    val realTankSize = if(hasDecorations) tankSize.times(0.8) else tankSize
    val size = currentFish.sum().plus(fishSize)

    return realTankSize - size >= 0
}